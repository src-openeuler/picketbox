%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                picketbox
Version:             5.1.0
Release:             1
Summary:             Security framework for Java Applications
License:             LGPLv2+
URL:                 https://picketbox.jboss.org
Source0:             https://github.com/%{name}/%{name}/archive/%{namedversion}/%{name}-%{namedversion}.tar.gz
BuildArch:           noarch
BuildRequires:       maven-local mvn(javax.persistence:persistence-api)
BuildRequires:       mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-release-plugin)
BuildRequires:       mvn(org.codehaus.mojo:javacc-maven-plugin) mvn(org.hibernate:hibernate-core:3)
BuildRequires:       mvn(org.hibernate:hibernate-entitymanager:3)
BuildRequires:       mvn(org.infinispan:infinispan-core) mvn(org.jboss:jboss-parent:pom:)
BuildRequires:       mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-annotations)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:       mvn(org.jboss.maven.plugins:maven-injection-plugin)
BuildRequires:       mvn(org.jboss.modules:jboss-modules) mvn(org.jboss.security:jbossxacml)
BuildRequires:       mvn(org.jboss.spec.javax.resource:jboss-connector-api_1.6_spec)
BuildRequires:       mvn(org.jboss.spec.javax.security.auth.message:jboss-jaspi-api_1.1_spec)
BuildRequires:       mvn(org.jboss.spec.javax.security.jacc:jboss-jacc-api_1.5_spec)
BuildRequires:       mvn(org.jboss.spec.javax.servlet:jboss-servlet-api_3.0_spec)
BuildRequires:       mvn(org.picketbox:picketbox-commons) xmvn
BuildRequires:       mvn(org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec)
BuildRequires:       mvn(javax.xml.bind:jaxb-api)
BuildRequires:       mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:       mvn(org.hibernate.javax.persistence:hibernate-jpa-2.1-api)
BuildRequires:       mvn(org.hibernate:hibernate-core)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools

%description
Java Security Framework that provides Java developers the following
functionality:
- Authentication Support
- Authorization Support
- Audit Support
- Security Mapping Support
- An Oasis XACML v2.0 compliant engine

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%autosetup -n %{name}-%{namedversion}
%pom_change_dep org.hibernate: ::3 security-jboss-sx/acl
%pom_remove_dep org.jboss.modules:jboss-modules 
%pom_add_dep_mgmt org.jboss.modules:jboss-modules:1.3.6.Final:compile 
%pom_remove_dep org.picketbox:common-spi security-jboss-sx/identity
%pom_add_dep org.picketbox:common-spi:'${project.version}':compile security-jboss-sx/identity
%pom_add_dep org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:1.0.2.Final security-spi/common
%pom_add_dep javax.xml.bind:jaxb-api:2.2.12-b141001.1542 security-jboss-sx/jbosssx
sed -i 's/${version.javacc.plugin}/2.6/g' security-jboss-sx/jbosssx/pom.xml

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build -f

%install
%mvn_install

%files -f .mfiles

%files javadoc -f .mfiles-javadoc

%changelog
* Thu Jan 25 2024 Ge Wang <wang__ge@126.com> - 5.1.0-1
- Update to version 5.1.0

* Tue Aug 01 2023 Ge Wang <wang__ge@126.com> - 4.9.6-2
- Compile with jdk11 due to jboss-modules updated

* Thu Aug 13 2020 zhanghua <zhanghua40@huawei.com> - 4.9.6-1
- package init
